package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);

    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
         
       System.out.println("Lets play!");
    int rounds = Integer.parseInt(sc.nextLine());

    for (int i = 0; i < rounds; i++) {
        playRockPaperScissors(sc);
    }
  }

static void playRockPaperScissors(Scanner scanner) {
    // Getting input from the user
    System.out.println("Your choice (rock/paper/scissors)");
    String playerMove = scanner.nextLine();

    // Getting input from the computer
    Random random = new Random();
    int randomNumber = random.nextInt(3);

    String computerMove;
    if (randomNumber == 0) {
        computerMove = "rock";
    } else if (randomNumber == 1) {
        computerMove = "paper";
    } else {
        computerMove = "scissors";
    }
    System.out.println("Computer chose " + computerMove + "!");

    // Print results
    if (playerMove.equals(computerMove)) {
        System.out.println("It's a tie!");
    } else if (playerWins(playerMove, computerMove)) {
        System.out.println("Human wins!");
    } else {
        System.out.println("Computer wins!");
    }
    while (true){

    System.out.println("Do you want to continue y or n");
    String c = scanner.nextLine();

   if(c.equalsIgnoreCase("y")){ 
      break; 
     } System.out.println("Bye Bye");

    }
}

static boolean playerWins(String playerMove, String computerMove) {
    if (playerMove.equals("rock")) {
        return computerMove.equals("scissors");
    } else if (playerMove.equals("paper")) {
        return computerMove.equals("rock");
    } else {
        return computerMove.equals("paper");
    }
  }


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
